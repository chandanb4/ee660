function [P_out] = Dist_new(old_Train,new_Train,P,nBins,nChunks)

% for ik = 1:50
%     E_dist = sqrt(sum(new_Train(iK,1:nBins)-old_Train(:)).^2);
%   %find I and Q
%    [Qi,Ii] = min(E_dist);
%    Q(ik) = Qi;
%    I(ik) = Ii;
% end
E_dist = zeros(nChunks,nChunks);
Q = zeros(1,nChunks);
I = zeros(1,nChunks);
for nIndex = 1:nChunks
    for oIndex = 1:nChunks
        E_dist(nIndex,oIndex) = sqrt(sum((new_Train(nIndex,1:nBins) - old_Train(oIndex,1:nBins)).^2));
    end
    [Q(nIndex),I(nIndex)] = min(E_dist(nIndex,:));
end

%update Q
Q_1 = 1 - exp(-Q);
Q_2 = 1./exp(Q_1);

%update P
P_1 = zeros(1,nChunks);
for i = 1:nChunks
    P_1(i) = (P(I(i))* Q_2(i));
end
N = sum(P_1);
P_out = P_1/N;


end