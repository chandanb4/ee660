clc
clear all
close all

load('preProcessing.mat');
%%
k=1;
for i=1:size(I,1)
     if I{i,2}==1
        
        I{k,2} = 1;
        k = k + 1;
     end
    if I{i,2}==2
        
        I{k,2} = -1;
        k = k + 1;
    end
end
%%
% feature = [];
% for i=1:size(I,1)
% t = I{i,1};[r c] = size(t);t = reshape(t,[1 r*c]);m = I{i,2};
% x = hist(im2double(t),50);
% x = [x m];feature = [feature ; x];
% end

feature = [];
for i=1:size(I,1)
I{i,1} = imresize(I{i,1}, [64 64]);
x = hog_feature_vector(I{i,1});m = I{i,2};
x = [x m];feature = [feature ; x];

end
save ('Feat_hist.mat','feature');




% %% to get weighted majority voting
% 
% w=[7 2 6];      %log(1/error)
% 
% votes = ['A' 'B' 'B'            %
%          'C' 'A' 'A'            %predictions of each hypothesis
%          'D' 'B' 'A'            % row wise
%          'A' 'A' 'C'];
% 
% options = ['A', 'B', 'C', 'D']';        %'-1' and '1'
% %'//Make a cube of the options that is number of options by m by n
% OPTIONS = repmat(options, [1, size(w, 2), size(votes, 1)]);
% 
% %//Compare the votes (streched to make surface) against a uniforma surface of each option
% B = bsxfun(@eq, permute(votes, [3 2 1]) ,OPTIONS);
% 
% %//Find a weighted sum
% W = squeeze(sum(bsxfun(@times, repmat(w, size(options, 1), 1), B), 2))'
% 
% %'//Find the options with the highest weighted sum
% [xx, i] = max(W, [], 2);
% options(i)