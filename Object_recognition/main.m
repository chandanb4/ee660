%% EE 660 Final Project: Part 1
% Multiple Object Recignition
% Recognizes 3 objects in a given scene (Car, Building and Tree)
% Code written by Chetan Sadhu and Chandan Basavaraju

%% Initialization
clc
clear
close all
addpath(genpath('D:\pmtk3-master\pmtk3-master'));

%% Read dataset

% Read Car Dataset
Cars1 = readDataset('temp','jpg');
Cars2 = readDataset('CarsDataset','jpg');
% Read Building dataset
Building = readDataset('Buildings','jpg');
% Read forest dataset
Forest = readDataset('Forest','jpg');
Tree = readDataset('tree2','jpg');

% Classifier
% 1 = SVM; 2 = Decision Tree; 3 = k-Nearest Neighbor
classifier = 1;

%% Feature Extraction
% generate label for multiclass SVM
if (classifier == 1)
    carLabel = [1 0 0];
    forestLabel = [0 1 0];
    buildingLabel = [0 0 1];
% generate label for decision tree and kNN
elseif (classifier == 2 || classifier == 3)
    carLabel = 1;
    forestLabel = 2;
    buildingLabel = 3;
end
% Feature extraction using HOG for all the objects
XCars1 = featExtract(Cars1, carLabel);
XCars2 = featExtract(Cars2, carLabel);
XBuilding = featExtract(Building, buildingLabel);
XForest = featExtract(Forest, forestLabel);
XTree = featExtract(Tree, forestLabel);
% Concatenating the results of feature extraction into 1 single matrix
X = [XCars2;XCars1;XForest;XTree;XBuilding];

%% Train Classifier
% X - feature vector and classifier = {1,2,3}
model = trainClassifier(X,classifier);
% save the final model file
save ('model.mat','model');
