%% readDataset
% path = Image data path
% format = format of the image (.jpg, .pgm, .png, etc.)
% I = cell array of images
% Code written by Chetan Sadhu and Chandan Basavaraju
%%
function I = readDataset(path,format)

Files = dir(strcat(path,'\*.',format));
I = cell(length(Files),1);

for k = 1:length(Files)
   img = imread(strcat(path,'\',Files(k).name));
   img = rgb2gray(img);
   img = imresize(img,[64 64]);
   I{k} = img;
end

end