%%
% This function reads the SIFT features, and finds match which is accepted
% only if its distance is less than distRatio times the distance to the
% second closest match.
% It returns the x, y, w and h values for both the objects.
function [x,y,w,h]= match(~, des1, ~,im2,des2,loc2)
% Coord1 and Coord2 are arrays containing x and y coordinates of matched
% keypoints respectively
Coord1 = [];
Coord2 = [];
% distRatio: Only keep matches in which the ratio of vector angles from the
% nearest to second nearest neighbor is less than distRatio.
distRatio = 0.6;

% For each descriptor in the first image, select its match to second image.
des2t = des2';                          % Precompute matrix transpose
for i = 1 : size(des1,1)
    % For efficiency in Matlab, it is cheaper to compute dot products between
    %  unit vectors rather than Euclidean distances.  Note that the ratio of
    %  angles (acos of dot products of unit vectors) is a close approximation
    %  to the ratio of Euclidean distances for small angles.
    dotprods = des1(i,:) * des2t;        % Computes vector of dot products
    [vals,indx] = sort(acos(dotprods));  % Take inverse cosine and sort results
    
    % Check if nearest neighbor has angle less than distRatio times 2nd.
    if (vals(1) < distRatio * vals(2))
        match(i) = indx(1);
    else
        match(i) = 0;
    end
end
%% Storing the x and y in Coord1 and Coord2 respectively
for i = 1: size(des1,1)
    if (match(i) > 0)
        %     line([loc1(i,2) loc2(match(i),2)+cols1], [loc1(i,1) loc2(match(i),1)], 'Color', 'c');
        Coord1 = [Coord1 loc2(match(i),1)];
        Coord2 = [Coord2 loc2(match(i),2)];
        %     Imm(round(loc2(match(i),1)),round(loc2(match(i),2)))=1;
    end
end
imshow(im2);
X=[Coord2;Coord1];
[~, y]=size(X);

if(y > 6)
    % k means clustering to identify cluster
    [IDX,~,~,D] = kmeans(X',N);
    Imm1=zeros(size(im2));
    Imm2=zeros(size(im2));
    for i=1:y
        if(IDX(i)==1&&D(i)>1000)
            Imm1(round(X(2,i)),round(X(1,i)))=1;
        end
        if(IDX(i)==2&&D(i)>1000)
            Imm2(round(X(2,i)),round(X(1,i)))=1;
        end
        
    end
end
num = sum(match > 0);
% returns the values to put a rectangle box for object 1
for i=1:N
    STATS = regionprops(Imm{i}, 'BoundingBox');
    x(i)=STATS.BoundingBox(1);
    y(i)=STATS.BoundingBox(2);
    w(i)=STATS.BoundingBox(3)+10;
    h(i)=STATS.BoundingBox(4)+10;
end
% returns the values to put a rectangle box for object 2
% STAT = regionprops(Imm2, 'BoundingBox');
% x2=STAT.BoundingBox(1);
% y2=STAT.BoundingBox(2);
% w2=STAT.BoundingBox(3)+10;
% h2=STAT.BoundingBox(4)+10;
fprintf('matches=%d\n',num);
