% im = appendimages(image1, image2)
%
% Return a new image that appends the two images side-by-side.

function im = appendimages(image)

% Select the image with the fewest rows and fill in enough empty rows
%   to make it the same height as the other image.
row_size = max(size(image{1:length(image)}),1);
for i=1:length(image)
    image{i} = padarray(image{i},row_size);
    im = [im image{i}];
end
% rows1 = size(image1,1);
% rows2 = size(image2,1);
% 
% if (rows1 < rows2)
%     image1(rows2,1) = 0;
% else
%     image2(rows1,1) = 0;
% end
% 
% % Now append both images side-by-side.
% im = [image1 image2];
