%%
clc
clear
close all
%%
% load('preProcessing.mat');
nBins = 50;
nChunks = 50;
% 
% %% Sift and Bag of words
% d = [];
% for i=1:size(I,1)
%     I_1 = single((I{i,1}));
%     [d1,f1] = vl_sift(I_1);
%     d = cat(2,d,d1);
%     d2{i} = d1;
% end
% 
% [centers, assignments] = vl_kmeans(double(d), 5);
% 
% for i = 1:size(I,1)
%     A = size(d2{i},2);
%     k = zeros(1,length(A));
%     for z = 1:A        
%         [~, k(z)] = min(vl_alldist(double(d2{i}(:,z)), centers)) ;
%     end
%     X_feature(i,:) = hist(k,nBins);
% end
% save ('Feat.mat','X_feature');
%%
load Feat_hist.mat;

%%
% Y = zeros(1,size(I,1));
% for z = 1:size(I,1)
%     Y(z) = I{z,2};
% end

% F_v = [X_feature Y'];
F_v = feature;

%% dividing the matrix into train and test

N     = size(feature,1);
k     = 3;
pos   = randperm(N);

% Edges
edges = 1:round(N/k):N+1;
if numel(edges) < k+1
    edges = [edges N+1];
end

% partition
prtA  = cell(k,1);
for ii = 1:k
    idx      = edges(ii):edges(ii+1)-1;
    prtA{ii} = F_v(pos(idx),:);
end

Train_mat_A =prtA{1,1};
Train_mat_B =prtA{2,1};
Train_mat = [Train_mat_A; Train_mat_B];
Test_mat =prtA{3,1};
%%
% Distribution function first time

Pd = makedist('uniform','lower',-1,'upper',+1);
A = -1:2/nChunks:1;
P = pdf(Pd,A);
P = P(1:nChunks);
% P_temp = P(1:nBins);


%%
% hypothesis first time

% model = svmtrain(Train_mat(1:nChunks,1:nBins),Train_mat(1:nChunks,nBins+1));
% % predicted_label = svmclassify(model,Test_mat(1:nChunks,1:nBins));
% w = (model.SupportVectors' * model.sv_indices);
% bias = model.Bias;

seg = datasample(Train_mat(1:50,:),50,'weights',P);
model = svmtrain(seg(1:50,nBins+1), seg(1:50,1:nBins));
[predicted_label, accuracy, decision_values] = svmpredict(Test_mat(1:50,nBins+1), Test_mat(1:50,1:nBins), model);
Predicted_final = predicted_label';
% w = (model.sv_coef' * full(model.SVs));
% 
% bias = -model.rho;





%% first time initialization
Et_final = [];
counter = nChunks + 1;
% old_Train = Train_mat(1:nChunks,1:nBins).*repmat(P,[nBins,1])';
old_Train = Train_mat(1:nChunks,1:nBins);
%%  put all that is below in for loop
for itr = 1:floor(size(Train_mat,1)/nChunks)
   predicted_label = predicted_label;
    if (counter > 1100)
        break;
    end
    new_Train = Train_mat(counter:nChunks+counter-1,1:nBins+1);
    P_t = Dist(old_Train, new_Train, P, nBins, nChunks);
    [P Et] = Distribution(P_t,predicted_label,Test_mat(1:50,nBins+1), nBins, nChunks);
    Et_final = [Et_final Et];
    % hypothesis
%     model = svmtrain(new_Train(1:nChunks,1:nBins),new_Train(1:nChunks,nBins+1));
    seg = datasample(new_Train(1:nChunks,:),50,'weights',P);
    model = svmtrain(seg(1:nChunks,nBins+1),seg(1:nChunks,1:nBins));
    [predicted_label, accuracy, decision_values] = svmpredict(Test_mat(1:50,nBins+1), Test_mat(1:50,1:nBins), model);

    Predicted_final = [Predicted_final; predicted_label'];
%     predicted_label = svmclassify(model,Test_mat(1:nChunks,1:nBins));
%     w = (model.SupportVectors' * model.SupportVectorIndices);
%     bias = model.Bias;
     counter = counter + nChunks;
     old_Train(:,1:nBins) = new_Train(:,1:nBins);
    old_Train(:,nBins + 1) = new_Train(:,nBins + 1);
end






%% to get weighted majority voting
Et_final = log(1./Et_final);
w=Et_final;      %log(1/error)

votes = Predicted_final(2:end,:);

[value,vote] = majority(votes,w);
% options = [-1,1]';        %'-1' and '1'
% %'//Make a cube of the options that is number of options by m by n
% OPTIONS = repmat(options, [1, size(w, 2), size(votes, 1)]);
% A = randperm(50);
% %//Compare the votes (streched to make surface) against a uniforma surface of each option
% B = bsxfun(@eq, permute(votes, A ) ,OPTIONS);
% 
% %//Find a weighted sum
% W = squeeze(sum(bsxfun(@times, repmat(w, size(options, 1), 1), B), 2)');
% 
% %'//Find the options with the highest weighted sum
% [xx, i] = max(W, [], 2);
% final_hyp=options(i);