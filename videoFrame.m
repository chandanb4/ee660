clc
clear all
close all

%% function to get individual video frames
% function videoFrame('Lion King - What did you do that for - the past can hurt.mp4')
% mkdir 'Database';
vid = VideoReader('F:\EE_660\Project\EE660_project\ee660\Video Database\Lion_King.mp4');
numFrames = vid.NumberOfFrames;
%for i = 1:numFrames
    i = 1500;
    I = read(vid,i);
    %im = rgb2gray(im);
    
    Igray = rgb2gray(I);
    
    %%Convert to binary
    Ibw = im2bw(Igray, 0.5);
    %Ibw = imclearborder(Ibw);
    %invert the image
    Ibw = ~Ibw;

    %Morphology
    Ifill = imfill(Ibw,'holes');
    Ifill1 = imfill(Igray, 'holes');
    %blob analysis
    [Ilabel, num(i)] = bwlabel(Ifill);
    [Ilabel1, centers] = adaptcluster_kmeans(Igray);
    %disp(num);
    
%     Iprops = regionprops(Ilabel1);
%     Ibox = [Iprops.BoundingBox];
% 
%     %reshape
%     Ibox = reshape(Ibox,[4 5]);
    figure(1);
    imshow(Ilabel1);
    
%     hold on;
% 
%     for cnt = 1:num(i)
%         rectangle('Position', Ibox(:,cnt),'EdgeColor', 'r');
%     end
% 
%     hold off;
%     cd 'Database';
%     imwrite(im,['Image' num2str(i) '.jpg']);
%     cd ..;
%end
% end