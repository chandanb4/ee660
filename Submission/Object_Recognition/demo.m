%% Demo.m : A demo code to show the working of object recognition.
% When the image is displayed the user is supposed to draw a rectangle box
% around the object (either car or building or tree)
% Code written by Chetan Sadhu and Chandan Basavaraju

%% Predict the class for the testing data
load model.mat
% read the test image
testImage = imread('testImages\test1.jpg');
% display the test image
while (1)
    pause(1);
    imshow(testImage);
    if (waitforbuttonpress)
        break;
    end
    hold on;
    % allow the user to put a draggable rectangle
    h1 = imrect;
    hold off;
    pos1 = h1.getPosition();
    I = imcrop(testImage, pos1);
    I = rgb2gray(I);
    I = imresize(I,[64 64]);
    
    % testimage feature vector
    testFeature = hog_feature_vector(I);
    
    % classify the test image
    if (classifier == 1)
        class1 = svmclassify(model(1), testFeature);
        class2 = svmclassify(model(2), testFeature);
        class3 = svmclassify(model(3), testFeature);
        class = 0;
        if (class1 == 1)
            class = class + 1;
            object = 'Car';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','green');
            imshow(RGB);
        end
        
        if (class2 == 1)
            class = class + 2;
            object = 'tree';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','blue');
            imshow(RGB);
        end
        
        if (class3 == 1)
            class = class + 4;
            object = 'building';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18);
            imshow(RGB);
        end
        if (class == 3 || class > 4 || class == 0)
            object = 'no_match';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','red');
            imshow(RGB);
        end
        
        %     RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18);
        %     imshow(RGB);
        
    elseif (classifier == 2)
        class = dtpredict(model,testFeature);
        if (class == 1)
            object = 'Car';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','green');
            imshow(RGB);
        end
        
        if (class == 2)
            object = 'tree';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','green');
            imshow(RGB);
        end
        
        if (class == 3)
            object = 'building';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','green');
            imshow(RGB);
        end
        if (class == 0)
            object = 'no_match';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','red');
            imshow(RGB);
        end
        
    elseif (classifier == 3);
        class = predict(model,testFeature);
        if (class == 1)
            object = 'Car';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','green');
            imshow(RGB);
        end
        
        if (class == 2)
            object = 'tree';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','green');
            imshow(RGB);
        end
        
        if (class == 3)
            object = 'building';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','green');
            imshow(RGB);
        end
        
        if (class == 0)
            object = 'no_match';
            RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18,'Color','red');
            imshow(RGB);
        end
    end
end
close all;