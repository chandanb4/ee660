%% Predict the class for the testing data

% read the test image
Files = dir(strcat('testImages\*.jpg'));
for ctr = 1:10
    for i=1:length(Files)
        testImage = imread(strcat('testImages\',Files(i).name));
        % display the test image
        imshow(testImage);
        while (1)
            pause(1);
            imshow(testImage);
            if (waitforbuttonpress)
                break;
            end
            hold on;
            % allow the user to put a draggable rectangle
            h1 = imrect;
            hold off;
            pos1 = h1.getPosition();
            I = imcrop(testImage, pos1);
            I = rgb2gray(I);
            I = imresize(I,[64 64]);
            
            % testimage feature vector
            testFeature = hog_feature_vector(I);
            
            % classify the test image
            if (classifier == 1)
                class1 = svmclassify(model(1), testFeature);
                class2 = svmclassify(model(2), testFeature);
                class3 = svmclassify(model(3), testFeature);
                class = 0;
                if (class1 == 1)
                    class = class + 1;
                    object = 'Car';
                end
                
                if (class2 == 1)
                    class = class + 2;
                    object = 'tree';
                end
                
                if (class3 == 1)
                    class = class + 4;
                    object = 'building';
                end
                if (class == 3 || class > 4 || class == 0)
                    object = 'no_match';
                end
                
                RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18);
                imshow(RGB);
                disp('class = ');
                disp(class);
                
            elseif (classifier == 2)
                class = dtpredict(model,testFeature);
                if (class == 1)
                    object = 'Car';
                end
                
                if (class == 2)
                    object = 'tree';
                end
                
                if (class == 3)
                    object = 'building';
                end
                if (class == 0)
                    object = 'no_match';
                end
                RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18);
                imshow(RGB);
                disp('class = ');
                disp(class);
                
            elseif (classifier == 3);
                class = predict(model,testFeature);
                if (class == 1)
                    object = 'Car';
                end
                
                if (class == 2)
                    object = 'tree';
                end
                
                if (class == 3)
                    object = 'building';
                end
                
                if (class == 0)
                    object = 'no_match';
                end
                
                RGB = insertObjectAnnotation(testImage,'rectangle',pos1,object,'TextBoxOpacity',0.9,'FontSize',18);
                imshow(RGB);
                disp('class = ');
                disp(class);
                
            end
        end
    end
end