%% trainClassifier
% feat = features with class labels
% classifier = 1:SVM, 2:Decision Tree 3:k-Nearest Neighbor
% model = trained classifier model
% Code written by Chetan Sadhu and Chandan Basavaraju

%%
function model = trainClassifier(feat,classifier)
[m,n] = size(feat);
addpath(genpath('D:\pmtk3-master\pmtk3-master'));

if (classifier == 1)
    
    X = feat(1:m,1:n-3);
    
    y = feat(1:m,n-2);
    model1 = svmtrain(X, y,'kernel_function','quadratic');
    
    y = feat(1:m,n-1);
    model2 = svmtrain(X, y,'kernel_function','quadratic');
    
    y = feat(1:m,n);
    model3 = svmtrain(X, y,'kernel_function','quadratic');
    
    model = [model1 model2 model3];

elseif (classifier == 2)
    X = feat(1:m,1:n-1);
    y = feat(1:m,n);
    model = dtfit(X,y,'maxdepth',10,'splitmeasure','GINI','randomFeatures',100);

elseif (classifier == 3)
   X = feat(1:m,1:n-1);
   y = feat(1:m,n);
   model = ClassificationKNN.fit(X,y);
end