%%
clc
clear
close all
addpath(genpath('D:\pmtk3-master\pmtk3-master'));

%%
% load('preProcessing.mat');
nBins = 1764;
nChunks = 50;
% 
%%
load Feat_hist.mat;

F_v = feature;

%% dividing the matrix into train and test
for iteration = 1:100
N     = size(feature,1);
k     = 5;
pos   = randperm(N);

% Edges
edges = 1:round(N/k):N+1;
if numel(edges) < k+1
    edges = [edges N+1];
end

% partition
prtA  = cell(k,1);
for ii = 1:k
    idx      = edges(ii):edges(ii+1)-1;
    prtA{ii} = F_v(pos(idx),:);
end

Train_mat_A =prtA{1,1};
Train_mat_B =prtA{2,1};
Train_mat_C =prtA{3,1};
Train_mat_D =prtA{4,1};
Train_mat_E =prtA{5,1};

Train_mat = [Train_mat_A; Train_mat_B;Train_mat_C; Train_mat_D;Train_mat_E];
Test_mat =prtA{3,1};
%%
% Distribution function first time

Pd = makedist('uniform','lower',-1,'upper',+1);
A = -1:2/nChunks:1;
P = pdf(Pd,A);
P = P(1:nChunks);
% P_temp = P(1:nBins);


%%
% hypothesis first time
seg = datasample(Train_mat(1:nChunks,:),100,'weights',P);
% model = svmtrain(seg(1:nChunks,1:nBins),seg(1:nChunks,nBins+1));
% predicted_label = svmclassify(model,Test_mat(1:nChunks,1:nBins));


%PMTK
  model{1,1} = svmFit(seg(1:nChunks,1:nBins),seg(1:nChunks,nBins+1));
% predicted_label  = svmPredict(model, Test_mat(1:nChunks,1:nBins));
% ytest = Test_mat(1:nChunks,nBins+1);
% errorRateGen = mean(predicted_label ~= ytest);



% w = (model.SupportVectors' * model.sv_indices);
% bias = model.Bias;
 
%   seg = datasample(Train_mat(1:nChunks,:),100,'weights',P);
%  model{1,1} = svmtrain(seg(1:nChunks,nBins+1), seg(1:nChunks,1:nBins));
% [predicted_label, accuracy, decision_values] = svmpredict(Test_mat(1:50,nBins+1), Test_mat(1:50,1:nBins), model);
 Predicted_final = [];
% w = (model.sv_coef' * full(model.SVs));
% 
% bias = -model.rho;





%% first time initialization
Et_final = [];
counter = nChunks + 1;
% old_Train = Train_mat(1:nChunks,1:nBins).*repmat(P,[nBins,1])';
old_Train = Train_mat(1:nChunks,1:nBins);
%%  put all that is below in for loop
for itr = 1:floor(size(Train_mat,1)/nChunks)
   
    if (counter > 1650)
        break;
    end
    new_Train = Train_mat(counter:nChunks+counter-1,1:nBins+1);
    P_t = Dist_new(old_Train, new_Train, P, nBins, nChunks);
    seg = datasample(new_Train(1:nChunks,:),100,'weights',P);
%  [predicted_label, accuracy, decision_values] = svmpredict(seg(:,nBins+1), seg(:,1:nBins), model{itr,1});
%PMTK
     predicted_label  = svmPredict(model{itr,1}, seg(1:nChunks,1:nBins));
    [P Et] = Distribution_new(P_t,predicted_label,Test_mat(1:nChunks,nBins+1), nBins, nChunks);
    ytest = Test_mat(1:nChunks,nBins+1);
 errorRateGen(itr) = mean(predicted_label ~= ytest);
    Et_final = [Et_final Et];
    % hypothesis
%     model = svmtrain(new_Train(1:nChunks,1:nBins),new_Train(1:nChunks,nBins+1));


% model = svmtrain(seg(1:nChunks,1:nBins),seg(1:nChunks,nBins+1));
% predicted_label = svmclassify(model,Test_mat(1:nChunks,1:nBins));

 model{itr+1,1} = svmFit(seg(1:nChunks,1:nBins),seg(1:nChunks,nBins+1));
%  predicted_label  = svmPredict(model, Test_mat(1:nChunks,1:nBins));ytest = Test_mat(1:nChunks,nBins+1);
% errorRateGen = mean(predicted_label ~= ytest);


% seg = datasample(new_Train(1:nChunks,:),50,'weights',P);
% LIBSVM
%     model{itr+1,1} = svmtrain(seg(1:nChunks,nBins+1),seg(1:nChunks,1:nBins));
   
    Predicted_final = [Predicted_final predicted_label];
%     predicted_label = svmclassify(model,Test_mat(1:nChunks,1:nBins));
%     w = (model.SupportVectors' * model.SupportVectorIndices);
%     bias = model.Bias;
     counter = counter + nChunks;
     old_Train(:,1:nBins) = new_Train(:,1:nBins);
    old_Train(:,nBins + 1) = new_Train(:,nBins + 1);
end


plot(Et_final);




%% 

 w=Et_final;      %log(1/error)
 [v ind] = max(w);
% 
 hypothesis_final = model{ind,1};
 
 %% test
correct_count = 0;
wrong_count = 0;
for i = 1:size(Test_mat,1)
    class = svmPredict(hypothesis_final,Test_mat(i,1:nBins));
    if (class == Test_mat(i,nBins+1))
        correct_count = correct_count + 1;
    else
        wrong_count = wrong_count + 1;
    end
end
accuracy(iteration) = correct_count/(correct_count + wrong_count);
end
acc_mean = mean(accuracy);
plot (accuracy);
acc_var = var(accuracy);
acc_max = max(accuracy);
acc_min = min(accuracy);