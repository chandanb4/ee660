%% updating distribution function
function [P_final Et] = Distribution_new(P_t,Prediction_label,Test_mat, nBins, nChunks)

% Prediction = sign(new_Train(:,1:nBins)*w+bias);
% [predicted_label, accuracy, decision_values] = svmpredict(Test_mat(1:50,nBins+1), Test_mat(1:50,1:nBins), model);

diff = abs(Prediction_label-Test_mat);
e1 = 0;e2 = 0;
A = (diff ~= 0);
for i=1:nChunks
    if A(i)~=0
        if Prediction_label(i)==1
        e1 = e1+P_t(i);
    else
        e2 = e2+P_t(i);
        end
    end
end
Et = log(1/e1)+log(1/e2);

et = sum(P_t(diff~=0));
if et==0
   et = 10^-2; 
end
P_update = zeros(1,nChunks);
for i=1:nChunks
    if diff == 0
        P_update(i) =P_t(i)*et;
    else
        P_update(i) = P_t(i);
    end
end

normalize = sum(P_update);
P_final = P_update/normalize;
end