The zipped folder contains four folders.
*Initialize pmtk to run the output files
-IMORL
	- BOV_vlfeat_new.m can be run to view the results of IMORL
-MLP
	- contains code that was written for MUlti layer Perceptron
-Object_Recognition
	-EE660_demo.mp4 is video demonstration of the classifier output
	-demo.m can be run to check the results
-Report
	-contains tex files and pdf of the final project report