%%
clc
clear all
close all
addpath('D:\EE660\Project\ee660\SIFT_Object_Detection');
%%
load preProcessing.mat
des1 = [];
for i=1:size(I,1)/10
    [im,des,loc] = sift(I{i,1});
    des1 = [des1;des];
end
[idx, ~, ~, D] = kmeans(des1',size(I,1));
Feat = hist(D,10);