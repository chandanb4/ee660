function [w ,bias] = SVM_Train (counter, Train_mat, Test_mat)


model = svmtrain(Train_mat(:,11), Train_mat(:,1:10));

[predicted_label, accuracy, decision_values] = svmpredict(Train_mat(:,11), Test_mat(:,1:10), model);

w = (model.sv_coef' * full(model.SVs));

bias = -model.rho;



end