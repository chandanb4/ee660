clc
clear
close all
%%
load('preProcessing.mat');
nBins = 20;

d = [];
for i=1:size(I,1)
    I_1 = single((I{i,1}));
    [d1,f1] = vl_sift(I_1);
    d = cat(2,d,d1);
    d2{i} = d1;
end

[centers, assignments] = vl_kmeans(double(d), 5);

for i = 1:size(I,1)
    A = size(d2{i},2);
    k = zeros(1,length(A));
    for z = 1:A        
        [~, k(z)] = min(vl_alldist(double(d2{i}(:,z)), centers)) ;
    end
    X_feature(i,:) = hist(k,nBins);
end
%%
save ('SVM_feat.mat','X_feature');