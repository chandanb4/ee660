% The XOR Example - Batch-Mode Training
%
% Author - Chandan and Chetan

clc
clear
close all

 load 'D:\EE660\Project\EE660\ee660\preProcessing.mat';
load 'D:\EE660\Project\EE660\ee660\Feat_hist.mat';

% p = 7;
% H = 14;
% m = 1;
% 
% mu = 1;
% alpha = 0.0051;
% 
% epoch = 40;
% MSEmin = 1e-20;
% 
Y = zeros(1,size(I,1));
for z = 1:size(I,1)
    Y(z) = I{z,2};
    if (Y(z) == 2)
        Y(z) = 1;
    else
        Y(z) = 0;
    end
    
end

addpath(genpath('C:\pmtk3-master'));

nhidden = 10;
setSeed(0);
% nVars = 2;
% nInstances = 400;
options.Display = 'none';
options.MaxIter = 500;
% X = X_feature;
% y = Y;
[N,D] = size(feature(:,1:50));
X1 = [ones(N,1) feature(:,1:50)];
lambda = 1e-2;

model = mlpFit(feature(:,1:50), feature(:,51), 'nhidden', nhidden, 'lambda', lambda, ...
  'fitOptions', options, 'method', 'schmidt');
 [yhat, py] = mlpPredict(model, feature(:,1:50));
nerr = sum(yhat' ~= y)/length(y)*100;
