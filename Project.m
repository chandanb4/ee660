clc
clear all
close all

%preprocessing

%%objects images of size H,W
H  =   ; W =  ;
X= zeros(m,(H*W));

% Dt-1 X and Y matric of dimention H*W with m fixed examples

for ii = 1:m
   X(ii,:) = reshape(Image_t-1(Strcat(Image_name)),1,(H*W)); 
end
% Y = Class_label column vector; 
Feature_vector = [X;Y];




%Hypothesis ht-1


%LibSVM

%P - uniform

Pd = makedist('uniform','lower',-1,'upper',+1);
A = -1:0.01:1;
P1 = pdf(Pd,A);

%Dt X and Y matrix
%m1 number of objects in t chunk
for ij = 1:m1
   X_t(ij,:) = reshape(Image_t(Strcat(Image_name)),1,(H*W)); 
end

%Distance between Dt-1 and Dt

%NewI is the new image that is obtained while testing  
X_trans = X_t';
for ik = 1:m2
   NewI = NewI';
   NewI = repmat(NewI,1,size(X_trans,2));
   E_dist = sqrt(sum(NewI-X_trans).^2);
  %find I and Q
   [Qi,Ii] = min(E_dist);
   Q(ik) = Qi;
   I(ik) = Ii;
end

%update Q
Q_1 = 1 - exp(-Q);
Q_2 = 1./exp(Q_1);

%update P
P = (P_i .* Q_2)/(normalization_const);
%update ht-1
