clc
clear
close all

load('preProcessing.mat');
nBins = 20;
load('SVM_feat.mat');

Y = zeros(1,size(I,1));
for z = 1:size(I,1)
    Y(z) = I{z,2};
    if (Y(z) == 2)
        Y(z) = 1;
    else
        Y(z) = 0;
    end
    
end

model = svmtrain(X_feature ,Y);

class = svmclassify(model,X_feature);

Accuracy = 100 - (sum(abs(class - Y')/size(Y,2)) * 100);